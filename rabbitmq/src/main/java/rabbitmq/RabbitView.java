package rabbitmq;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class RabbitView extends JFrame {

	private JPanel contentPane;
	private JTextField textYear;
	private JTextField textTitle;
	private JTextField textPrice;
	private JButton sPrice;
	JTextArea textArea ;
	
	public RabbitView() {
		
		setTitle("Rabbit Swing");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 300, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel insertDetails = new JLabel("Insert new DVD details");
		insertDetails.setBounds(10, 11, 120, 14);
		contentPane.add(insertDetails);

		JLabel iYear = new JLabel("year:");
		iYear.setBounds(10, 36, 60, 14);
		contentPane.add(iYear);

		JLabel iengineSize = new JLabel("title:");
		iengineSize.setBounds(10, 61, 60, 14);
		contentPane.add(iengineSize);

		JLabel iPrice = new JLabel("price");
		iPrice.setBounds(10, 86, 46, 14);
		contentPane.add(iPrice);

		textYear = new JTextField();
		textYear.setBounds(80, 33, 86, 20);
		contentPane.add(textYear);
		textYear.setColumns(10);

		textTitle = new JTextField();
		textTitle.setBounds(80, 58, 86, 20);
		contentPane.add(textTitle);
		textTitle.setColumns(10);

		textPrice = new JTextField();
		textPrice.setBounds(80, 83, 86, 20);
		contentPane.add(textPrice);
		textPrice.setColumns(10);

	
		
		sPrice = new JButton("Send");
		sPrice.setBounds(10, 162, 89, 23);
		contentPane.add(sPrice);
		
	
		
	}
	
	
	public void sendListener(ActionListener e) {
		sPrice.addActionListener(e);
	}

	
	

	public String getDVDYear() {
		return textYear.getText();
	}
	
	public String getDVDTitle() {
		return textTitle.getText();
	}

	public String getDVDPrice() {
		return textPrice.getText();
	}
	
	

	
}