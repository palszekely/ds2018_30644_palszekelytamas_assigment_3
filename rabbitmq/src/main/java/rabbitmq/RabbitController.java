package rabbitmq;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;




public class RabbitController  {
	
	private RabbitView rView;


	public RabbitController() {
		rView = new RabbitView();
		rView.setVisible(true);


		rView.sendListener(new MyActionListener());
	
	}

	
	 class MyActionListener implements ActionListener {
 @Override
		public void actionPerformed(ActionEvent e) {
			String myyear = rView.getDVDYear();
			String mytitle = rView.getDVDTitle();
			String myprice = rView.getDVDPrice();
			int my=Integer.parseInt(myyear);
			double myp= Double.parseDouble(myprice);
            DVD c=new DVD(mytitle,my,myp);
            try{  
	    		  
            	 String queue_name = "myQueue";
            	 
            	          ConnectionFactory factory = new ConnectionFactory();
            	          factory.setHost("localhost");

            	          Connection con = factory.newConnection();
            	          Channel rabbitChannel = con.createChannel();
            	          rabbitChannel.queueDeclare(queue_name, false, false, false, null);
            	          
            	        	 
            	            String msg = c.toString();
            	            rabbitChannel.basicPublish("",queue_name, null, msg.getBytes());
            	           
            	            System.out.println(" �.Sent '" + msg + "'");
            	            Thread.sleep(1000);
            	        
            	        rabbitChannel.close();
            	        con.close();
    	    	
	              
	           
	        
	    		 
	    		  
	      } catch (Exception er) {
	         System.err.println("Client exception: " + e.toString()); 
	         er.printStackTrace(); 
	      } 
			

				 
			
		}
	}
	
	
	
	
	
	


	
	
}

