package rabbitmq;

public class Subscriber {
	
	public int id;
	public String email;
	public String nume;
	
	public Subscriber(int i,String e,String n) {
		this.id=i;
		this.email=e;
		this.nume=n;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	
	
}
